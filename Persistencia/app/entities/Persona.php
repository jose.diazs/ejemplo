<?php

class Persona {
  private $id;
  private  $nombre;
  private $apellido;
  private $documento;
  private $genero;
  private $nacimiento;
  private $telefono;
  
  function __construct() {
      
  }

  
  function getId() {
      return $this->id;
  }

  function getNombre() {
      return $this->nombre;
  }

  function getApellido() {
      return $this->apellido;
  }

  function getDocumento() {
      return $this->documento;
  }

  function getGenero() {
      return $this->genero;
  }

  function getNacimiento() {
      return $this->nacimiento;
  }

  function getTelefono() {
      return $this->telefono;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setNombre($nombre) {
      $this->nombre = $nombre;
  }

  function setApellido($apellido) {
      $this->apellido = $apellido;
  }

  function setDocumento($documento) {
      $this->documento = $documento;
  }

  function setGenero($genero) {
      $this->genero = $genero;
  }

  function setNacimiento($nacimiento) {
      $this->nacimiento = $nacimiento;
  }

  function setTelefono($telefono) {
      $this->telefono = $telefono;
  }



}



<?php

namespace App\Entities;  

class Aerolinea {
    
    private $nombre;
    private $nit;
    
    
    function __construct() {
    
       
    }

    function getNombre() {
        return $this->nombre;
    }

    function getNit() {
        return $this->nit;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setNit($nit) {
        $this->nit = $nit;
    }


}

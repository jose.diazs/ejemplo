<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Aereopuerto
 *
 * @author Ivan
 */
namespace App\Entities;


class Aereopuerto {
    private $id_aereopuerto;
    private $ciudad;
    private $nombre;
    function __construct() {
        
    }
    function getId_aereopuerto() {
        return $this->id_aereopuerto;
    }

    function getCiudad() {
        return $this->ciudad;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId_aereopuerto($id_aereopuerto) {
        $this->id_aereopuerto = $id_aereopuerto;
    }

    function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }


}

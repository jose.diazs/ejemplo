<?php

namespace App\Entities;  

class Pasajero  {

    private $id_pasajero;
    private $nombre;
    private $apellido;
    private $genero;
    private $nacimiento;
    private $documento;
    private $telefono;
    private $pasaporte;
    private $millas;
    
    function __construct() {
        
    }

    
    function getId_pasajero() {
        return $this->id_pasajero;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getGenero() {
        return $this->genero;
    }

    function getNacimiento() {
        return $this->nacimiento;
    }

    function getDocumento() {
        return $this->documento;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getPasaporte() {
        return $this->pasaporte;
    }

    function getMillas() {
        return $this->millas;
    }

    function setId_pasajero($id_pasajero) {
        $this->id_pasajero = $id_pasajero;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setNacimiento($nacimiento) {
        $this->nacimiento = $nacimiento;
    }

    function setDocumento($documento) {
        $this->documento = $documento;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setPasaporte($pasaporte) {
        $this->pasaporte = $pasaporte;
    }

    function setMillas($millas) {
        $this->millas = $millas;
    }


    
    

}

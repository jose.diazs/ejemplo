<?php

class Avion {
    private $id_avion;
    private $nombre;
    private $nit;
    private $capacidad;
    private $longitud;
    private $envergadura;
    private $velocidad_crucero;
    private $peso;
    
    function __construct() {
        
    }

    function getId_avion() {
        return $this->id_avion;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getNit() {
        return $this->nit;
    }

    function getCapacidad() {
        return $this->capacidad;
    }

    function getLongitud() {
        return $this->longitud;
    }

    function getEnvergadura() {
        return $this->envergadura;
    }

    function getVelocidad_crucero() {
        return $this->velocidad_crucero;
    }

    function getPeso() {
        return $this->peso;
    }

    function setId_avion($id_avion) {
        $this->id_avion = $id_avion;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setNit($nit) {
        $this->nit = $nit;
    }

    function setCapacidad($capacidad) {
        $this->capacidad = $capacidad;
    }

    function setLongitud($longitud) {
        $this->longitud = $longitud;
    }

    function setEnvergadura($envergadura) {
        $this->envergadura = $envergadura;
    }

    function setVelocidad_crucero($velocidad_crucero) {
        $this->velocidad_crucero = $velocidad_crucero;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }


}

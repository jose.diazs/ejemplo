<?php
namespace App\Repositories;

use PDO;
use Config\Conn;

class AvionRepositorio  extends Conn{

    public function getAll() {
        $sql = " select id_avion,nombre,nit,capacidad,longitud,"
                . " envergadura,velocidad_crucero,peso "
                . " from avion ";

        $resource = $this->_conn->prepare($sql);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }

    public function getById($id_avion) {
        $sql = " select id_avion,nombre,nit,capacidad,longitud,"
                . " envergadura,velocidad_crucero,peso "
                . " from avion "
                . " where id_avion=:id_avion";

        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':id_avion', $id_avion);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        if (isset($rows)) {
            return $rows[0];
        }
        return null;
    }

    public function delete($id_avion) {
        $sql = " delete from avion "
                . " where id_avion=:id_avion ";
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':id_avion', $id_avion);
        $resource->execute();
    }

    
    public function update($avion){
        print_r($avion);
        $sql = " update avion"
                . "  set nombre=:nombre, "
                . "      nit=:nit,"
                . "      capacidad=:capacidad,"
                . "      longitud=:longitud,"
                . "      envergadura=:envergadura,"
                . "      velocidad_crucero=:velocidad_crucero,"
                . "      peso0=:peso "
                . "  where id_avion=:id_avion ";
                  $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $avion->getNombre());
        $resource->bindValue(":nit", $avion->getNit());
        $resource->bindValue(":capacidad", $avion->getCapacidad());
        $resource->bindValue(":longitud", $avion->getLongitud());
        $resource->bindValue(":envergadura", $avion->getEnvergadura());
        $resource->bindValue(":velocidad_crucero", $avion->getVelocidad_crucero());
        $resource->bindValue(":peso", $avion->getPeso());
        $resource->bindValue(":id_avion", $avion->getId_pasajero());
        $resource->execute();
    }
    
    public function save($pasajero) {
        $sql = " insert into "
                . "  avion (id_avion,nombre,nit,capacidad,longitud,envergadura,velocidad_crucero,peso)"
                . "  values (:id_avion,:nombre,:nit,:capacidad,:longitud,:envergadura,:velocidad_crucero,:peso)";
        
        $resource->bindValue(":nombre", $avion->getNombre());
        $resource->bindValue(":nit", $avion->getNit());
        $resource->bindValue(":capacidad", $avion->getCapacidad());
        $resource->bindValue(":longitud", $avion->getLongitud());
        $resource->bindValue(":envergadura", $avion->getEnvergadura());
        $resource->bindValue(":velocidad_crucero", $avion->getVelocidad_crucero());
        $resource->bindValue(":peso", $avion->getPeso());
        $resource->bindValue(":id_avion", $avion->getId_pasajero());
        $resource->execute();
    }

}

<?php
namespace App\Repositorios;

use PDO;
use Config\Conn;

class PasajeroRepositorio  extends Conn{

    public function getAll() {
        $sql = " select id_pasajero,nombre, apellido,documento,genero,"
                . " nacimiento,telefono,pasaporte,millas "
                . " from pasajero ";

        $resource = $this->_conn->prepare($sql);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }

    public function getById($id_pasajero) {
        $sql = " select id_pasajero,nombre, apellido,documento,genero,"
                . " nacimiento,telefono,pasaporte,millas "
                . " from pasajero "
                . " where id_pasajero=:id_pasajero";

        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':id_pasajero', $id_pasajero);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        if (isset($rows)) {
            return $rows[0];
        }
        return null;
    }

    public function delete($id_pasajero) {
        $sql = " delete from pasajero "
                . " where id_pasajero=:id_pasajero ";
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':id_pasajero', $id_pasajero);
        $resource->execute();
    }

    
    public function update($pasajero){
        print_r($pasajero);
        $sql = " update pasajero"
                . "  set nombre=:nombre, "
                . "      apellido=:apellido,"
                . "      documento=:documento,"
                . "      genero=:genero,"
                . "      telefono=:telefono,"
                . "      pasaporte=:pasaporte,"
                . "      millas=:millas "
                . "  where id_pasajero=:id_pasajero ";
                  $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $pasajero->getNombre());
        $resource->bindValue(":apellido", $pasajero->getApellido());
        $resource->bindValue(":documento", $pasajero->getDocumento());
        $resource->bindValue(":genero", $pasajero->getGenero());
        $resource->bindValue(":telefono", $pasajero->getTelefono());
        $resource->bindValue(":pasaporte", $pasajero->getPasaporte());
        $resource->bindValue(":millas", $pasajero->getMillas());
        $resource->bindValue(":id_pasajero", $pasajero->getId_pasajero());
        $resource->execute();
    }
    
    public function save($pasajero) {
        $sql = " insert into "
                . "  pasajero (nombre, apellido,documento,genero,telefono,pasaporte,millas)"
                . "  values (:nombre,:apellido,:documento,:genero,:telefono,:pasaporte,:millas)";
        
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $pasajero->getNombre());
        $resource->bindValue(":apellido", $pasajero->getApellido());
        $resource->bindValue(":documento", $pasajero->getDocumento());
        $resource->bindValue(":genero", $pasajero->getGenero());
        $resource->bindValue(":telefono", $pasajero->getTelefono());
        $resource->bindValue(":pasaporte", $pasajero->getPasaporte());
        $resource->bindValue(":millas", $pasajero->getMillas());
        $resource->execute();
    }

}

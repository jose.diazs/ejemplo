<?php

namespace App\Repositories;

use PDO;
use Config\Conn;

class AereolineaRepositorio extends Conn{

    public function getAll() {
        $sql = " select nit,nombre"
                . " from aereolinea ";

        $resource = $this->_conn->prepare($sql);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }
    public function getById($nit) {
        $sql = " select nit,nombre"
                . " from aereolinea "
                . " where nit=:nit";

        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':nit', $nit);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        if (isset($rows)) {
            return $rows[0];
        }
        return null;}
     public function delete($nit) {
        $sql = " delete from aereolinea "
                . " where nit=:nit ";
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(':nit', $nit);
        $resource->execute();
    }

      public function update($aereolinea){
        print_r($aereolinea);
        $sql = " update aereolinea"
                . "  set nombre=:nombre, "
                . "  where nit=:nit";
                  $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $aereolinea->getNombre());
        $resource->bindValue(":nit", $aereolinea->getNit());
        $resource->execute();
    }
     public function save($aereolinea) {
        $sql = " insert into "
                . "  aereolinea (nit,nombre)"
                . "  values (:nit,:nombre)";
        
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $aereolinea->getNombre());
        $resource->execute();
    }  
}


<?php

namespace App\Repositories;

use PDO;
use Config\Conn;

class AeropuertoRepositorio extends Conn {

    public function getAll() {
        $sql = " select id_aereopuerto,ciudad,nombre"
                . " from aereopuerto ";

        $resource = $this->_conn->prepare($sql);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }
    
     public function getAeropuerto($nombreAeropuerto) {
         echo "jajajaja";
         exit();
        $nombre = '%'.$nombreAeropuerto.'%';
        $sql = " select * "
                . " from aereopuerto where nombre like :nombre";

        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $nombre);
        $resource->execute();
        return $resource->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getById($nit) {
            $sql = " select id_aeropuerto,ciudad,nombre"
                    . " from aereopuerto "
                    . " where id-aereopuerto:=id_aereopuerto";

            $resource = $this->_conn->prepare($sql);
            $resource->bindValue(':nombre', $nit);
            $resource->execute();
            $rows = $resource->fetchAll(PDO::FETCH_OBJ);
            if (isset($rows)) {
                return $rows[0];
            }
            return null;
        }

        public function delete($nit) {
            $sql = " delete from aereopuerto "
                    . " where id-aereopuerto=:idaereopuerto ";
            $resource = $this->_conn->prepare($sql);
            $resource->bindValue(':nit', $nit);
            $resource->execute();
        }

        public function update($aereolinea) {
            print_r($aereolinea);
            $sql = " update aereopuerto"
                    . "  set nombre=:nombre, "
                . "  set ciudad=:ciudad, "
                . "  where id_aereopuerto=:id_aereopuerto";
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":ciudad", $aereolinea->getCiudad());
        $resource->bindValue(":nombre", $aereolinea->getNombre());
        $resource->bindValue(":id_aereopuerto", $aereolinea->getId_Aereopuerto());
        $resource->execute();
    }

     public function save($aereolinea) {
        $sql = " insert into "
                . "  aereopuerto(id_aereopuerto,ciudad,nombre)"
                . "  values (:id_aereopuerto,:ciudad,:nombre)";
        
        $resource = $this->_conn->prepare($sql);
        $resource->bindValue(":nombre", $aereolinea->getNombre());
        $resource->execute();
    }  
}

<?php

namespace App\Controller;

include_once  "../../config/loader.php";

use App\Entities\Pasajero;
use App\Repositories\PasajeroRepositorio;


class PasajeroController {

    static function save($pasajero) {
        $repo = new PasajeroRepositorio();
        $repo->save($pasajero);
    }

    static function update($pasajero) {
        $repo = new PasajeroRepositorio();
        $repo->update($pasajero);
    }

    static function delete($id_pasajero) {
        $repo = new PasajeroRepositorio();
        $repo->delete($id_pasajero);
    }

    static function getAll() {
        $repo = new PasajeroRepositorio();
        return $repo->getAll();
    }

    static function getById($id_pasajero) {
        $repo = new PasajeroRepositorio();
        $repo->getById($id_pasajero);
    }

}

function getPasajero() {
    $pasajero = new Pasajero();
    if (isset($_POST['id_pasajero']))
        $pasajero->setId_pasajero($_POST['id_pasajero']);
    $pasajero->setNombre($_POST['nombre']);
    $pasajero->setApellido($_POST['apellido']);
    $pasajero->setDocumento($_POST['documento']);
    $pasajero->setNacimiento($_POST['nacimiento']);
    $pasajero->setTelefono($_POST['telefono']);
    $pasajero->setPasaporte($_POST['pasaporte']);
    $pasajero->setMillas($_POST['millas']);
    $pasajero->setGenero($_POST['genero']);
    return $pasajero;
}

if (isset($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 'save':
            $pasajero = getPasajero();
            PasajeroController::save($pasajero);
            break;
        case 'update':
            echo "update";
            $pasajero = getPasajero();
            PasajeroController::update($pasajero);
            break;
        case 'delete' :
            $id = $_POST['id_pasajero'];
            PasajeroController::delete($id);
            break;
        
    }
}
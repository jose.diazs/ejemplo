<?php

namespace App\Controller;

include_once  "../../config/loader.php";

use App\Entities\Vuelo;
use App\Repositories\VueloRepositorio;


class VueloController {

    static function save($Vuelo) {
        $repo = new VueloRepositorio();
        $repo->save($Vuelo);
    }

    static function update($Vuelo) {
        $repo = new VueloRepositorio();
        $repo->update($Vuelo);
    }

    static function delete($id_Vuelo) {
        $repo = new VueloRepositorio();
        $repo->delete($id_Vuelo);
    }

    static function getAll() {
        $repo = new VueloRepositorio();
        return $repo->getAll();
    }

    static function getById($id_Vuelo) {
        $repo = new VueloRepositorio();
        $repo->getById($id_Vuelo);
    }

}

function getVuelo() {
    $Vuelo = new Vuelo();
    if (isset($_POST['id_Vuelo']))
        $Vuelo->setId_Vuelo($_POST['id_Vuelo']);
    $Vuelo->setNombre($_POST['nombre']);
    $Vuelo->setApellido($_POST['apellido']);
    $Vuelo->setDocumento($_POST['documento']);
    $Vuelo->setNacimiento($_POST['nacimiento']);
    $Vuelo->setTelefono($_POST['telefono']);
    $Vuelo->setPasaporte($_POST['pasaporte']);
    $Vuelo->setMillas($_POST['millas']);
    $Vuelo->setGenero($_POST['genero']);
    return $Vuelo;
}

if (isset($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 'save':
            $Vuelo = getVuelo();
            VueloController::save($Vuelo);
            break;
        case 'update':
            echo "update";
            $Vuelo = getVuelo();
            VueloController::update($Vuelo);
            break;
        case 'delete' :
            $id = $_POST['id_Vuelo'];
            VueloController::delete($id);
            break;

    }
}

<?php
namespace App\Controller;

include_once  "../../config/loader.php";

use App\Entities\Aereopuerto;
use App\Repositories\AeropuertoRepositorio;

class AereopuertoContrller {

    static function save($aereolinea) {
        $repo = new AeropuertoRepositorio();
        $repo->save($aereolinea);
    }

    static function update($aereolinea) {
        $repo = new AeropuertoRepositorio();
        $repo->update($aereolinea);
    }

    static function delete($id_pasajero) {
        $repo = new AeropuertoRepositorio();
        $repo->delete($nit);
    }

    static function getAll() {
        $repo = new AeropuertoRepositorio();
        return $repo->getAll();
    }

    static function getById($id_pasajero) {
        $repo = new AeropuertoRepositorio();
        return $repo->getById($nit);
    }

    static function getByNombre($nombre) {
        $repo = new AeropuertoRepositorio();
        return $repo->getAeropuerto($nombre);
    }

}

function getAeropuerto() {
    $Aereolinea = new Aereopuerto();
    if (isset($_POST['id_aereopuerto']))
        $Aereolinea->setId_aereopuerto($_POST['id-aereopuerto']);
    $Aereolinea->setCiudad($_POST['ciudad']);
    $Aereolinea->setNombre($_POST['nombre']);
    return $Aereolinea;
}

if (isset($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 'save':
            $Aereolinea = getAeropuerto();
            AereopuertoContrller::save($Aereolinea);
            break;
        case 'update':
            echo "update";
            $Aereolinea = getAeropuerto();
            AereopuertoContrller::update($Aereolinea);
            break;
        case 'delete' :
            $id = $_POST['id_aereopuerto'];
            AereopuertoContrller::delete($id);
            break;
        case 'buscar' :
//            echo "todo bien";
//            exit();
            $nombre = $_POST['nombreAe'];
            echo AereopuertoContrller::getByNombre($nombre);
            break;
    }
}
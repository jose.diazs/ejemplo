<?php

namespace App\Controller;

include_once  "../../config/loader.php";

use App\Entities\Avion;
use App\Repositories\   AvionRepositorio;


class PasajeroController {

    static function save($avion) {
        $repo = new \AvionRepositorio();
        $repo->save($avion);
    }

    static function update($avion) {
        $repo = new PasajeroRepositorio();
        $repo->update($avion);
    }

    static function delete($id_avion) {
        $repo = new AvionRepositorio();
        $repo->delete($id_avion);
    }

    static function getAll() {
        $repo = new AvionRepositorio();
        return $repo->getAll();
    }

    static function getById($id_avion) {
        $repo = new AvionRepositorio();
        $repo->getById($id_avion);
    }

}

function getPasajero() {
    $avion = new Avion();
    if (isset($_POST['id_avion']))
        $avion->setId_avion($_POST['id_avion']);
    $avion->setNombre($_POST['nombre']);
    $avion->setNit($_POST['nit']);
    $avion->setCapacidad($_POST['capacidad']);
    $avion->setLongitud($_POST['longitud']);
    $avion->setEnvergadura($_POST['envergadura']);
    $avion->setVelocidad_crucero($_POST['velocidad_crucero']);
    $avion->setPeso($_POST['peso']);
    return $avion;
}

if (isset($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 'save':
            $avion = getAvion();
            PasajeroController::save($avion);
            break;
        case 'update':
            echo "update";
            $avion = getAvion();
            PasajeroController::update($avio);
            break;
        case 'delete' :
            $id = $_POST['id_avion'];
            PasajeroController::delete($id);
            break;
        
    }
}
<?php
include_once "../../../config/loader.php";

use Controller\AereopuertoContrller;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <input type="text" name="nombre" id="nombre"/>
        <input type="submit" value="buscar" id="buscar" />
        <script>
            $('#buscar').click(function () {
                let nombre = $('#nombre').val();
                console.log("escribio: " + nombre)
                $.ajax(
                        {
                            url: "../../../app/controller/AereopuertoContrller.php",
                            data: {
                                accion: "buscar",
                                nombreAe: nombre
                            },
                            type: "POST",
                            dataType: "text"
                        }
                ).done(function (data) {
                    console.log(data);
                }).fail(function () {
                    console.log("Error")
                })
            });
        </script>

    </body>
</html>

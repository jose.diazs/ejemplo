<?php

namespace Config;

use PDO;

class Conn {
    private $_motor;
    private $_servidor;
    private $_usuario;
    private $_clave;
    private $_nombreBase;
    protected $_conn;

    public function __construct() {
        $CONFIG = parse_ini_file("env.php");
        $this->_servidor = $CONFIG["DB_HOST"];
        $this->_nombreBase = $CONFIG["DB_DATABASE"];
        $this->_usuario = $CONFIG["DB_USERNAME"];
        $this->_clave = $CONFIG["DB_PASSWORD"];
        $this->_motor = $CONFIG["DB_CONNECTION"];
        $this->conectar();
    }

    public function conectar() {
        switch ($this->_motor) {
            case "mysql":
                $codificacion = "SET NAMES \"UTF8\"";
                $cadena = "mysql::host={$this->_servidor};dbname={$this->_nombreBase}";
                $arreglo = array(PDO::MYSQL_ATTR_INIT_COMMAND => $codificacion);
                $this->_conn = new PDO($cadena, $this->_usuario, $this->_clave, $arreglo);
                break;
            case "pgsql":
                $cadena = "pgsql:host={$this->_servidor};dbname={$this->_nombreBase}";
                $this->_conn = new PDO($cadena, $this->_usuario, $this->_clave);
                break;
            default :
                echo "No se reconocer el driver de la base de datos";
                exit();
                break;
        }
    }

}

?>

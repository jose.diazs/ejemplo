<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


function autoLoad($name) {
    $configPath = "config";
    $currentPath = str_replace("\\", "/", __DIR__);
    $projectPath = str_replace($configPath, "", $currentPath);
    $myClass = strtolower($name);
    $classPath = str_replace("\\", "/", $myClass);
    $finalPath = $projectPath."".$classPath.".php";
    echo $finalPath;
    exit();
    require ($finalPath);     
}

spl_autoload_register("autoLoad");
?>